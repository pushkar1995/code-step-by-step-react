import './App.css'
// import ClickEF from './components/ClickEvent&F'
// import StateInFC from './components/StateInFC';
// import StateInFC from './components/StateInCC';
// import PropsWithFC from './components/propsExample/PropsWithFC';
// import InputBox from './components/inputBoxExample/InputBox';
import Toggle from './components/Toggle'

function App() {

  return (
    <div className="App">
      {/* <ClickEF /> */}
      {/* <StateInFC /> */}
      {/* <StateInFC /> */}
      {/* <PropsWithFC /> */}
      {/* <InputBox /> */}
      <Toggle />
    </div>
  );
}

export default App;
