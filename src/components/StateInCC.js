import React, { Component } from 'react'

class StateInCC extends Component {
    constructor() {
        super()  //super is the functionality of JS not React
        this.state = {
            // data: 'Pushkar'
            data: 1
        }
    }
    apple () {
        // alert('This is Appple')

        //To Update data: 'Pushkar'
        // this.setState({data: 'Gautam'})

        //
        this.setState({data: this.state.data + 1})
    }
    render() {
        return (
            <div>
                <h1>{this.state.data}</h1>
                <button onClick={()=>{this.apple()}}>Update Data</button>
                {/* Arrow function must be used in Class Component Other Wise Function is called without button click */}
            </div>
        )
    }
}

export default StateInCC
