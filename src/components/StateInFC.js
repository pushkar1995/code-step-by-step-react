import { useState } from 'react'


function StateInFC() {
//   const [data, setData] = useState('Pushkar')

//   function updateData() {
//       setData('Gautam')
//   }

    const [data, setData] = useState(0)

    function updateData() {
        setData(data + 1)
    }
  console.warn('________')
    return (
        <div>
            <h1>{data}</h1>
            <button onClick={updateData}>Update Data</button>
        </div>
    )
}

export default StateInFC

// NOT A GOOD PRACTICE

// function StateInFC() {
//     let data='pushkar'

//     function updataData() {
//         data='gautam'
//         alert(data)
//     }
//     console.warn('________')
//     return (
//         <div>
//             <h1>{data}</h1>
//             <button onClick={updataData}>Update Data</button>
//         </div>
//     )
// }

// export default StateInFC
