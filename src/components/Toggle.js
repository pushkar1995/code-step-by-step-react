import React,{ useState } from 'react'

function Toggle() {
    const [status,setStatus] = useState(true)

    return (
        <div>
            {
            status ?
            <h1>Hello World</h1>
            : null
            }
            {/* Toogle with 2 Buttons */}
            {/* <button onClick={() => setStatus(false)}>Hide</button>
            <button onClick={() => setStatus(true)}>Show</button> */}

            {/* Toogle with single Button */}
            <button onClick={() => setStatus(!status)}>TOOGLE</button>

        </div>
    )
}

export default Toggle
