import React from 'react'

// alert('Hello')
// this function is directly called

 function ClickEventAndFunc() {

     let data="Hello World"
    // use state and props instead like this using the variable like this

     function buttonClick() {
        // data = "Hi"
        // use state and props instead like this using the variable like this
        alert(data)
        // alert('function called')
        
     }
    return (
        <div>
            <h1>{data}</h1>
          
            {/* <button onClick={alert('I am called Directly')}>Click Me!!</button> */}

            {/* Use arrow function to call function directly inline*/}
            {/* <button onClick={() => alert('I am called While button is clicked')}>Click Me!!</button> */}

            <button onClick={() => buttonClick()}>Click Me!!</button>

            {/* <button onClick={buttonClick()}>Click Me!!</button> */}
            {/* this function is directly called because small bracket is used after */}

            {/* <button onClick={buttonClick}>Click Me!!</button> */}
            {/* Function should be called like this */}
        </div>
    )
}

export default ClickEventAndFunc

