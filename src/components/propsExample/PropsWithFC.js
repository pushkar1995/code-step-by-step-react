import React from 'react'

import Student from './Student'

function PropsWithFC() {
    return (
        <div>
            Hello
            <Student name='Pushkar' />
        </div>
    )
}

export default PropsWithFC
